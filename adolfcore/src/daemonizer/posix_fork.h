//17copyright   

 

#pragma once
#include <string>

#ifndef WIN32

namespace posix {

void fork(const std::string & pidfile);

}

#endif
