//17copyright   

                                                
// 
// Parts of this file are originally copyright (c) 2012-2013 The Cryptonote developers

#pragma once

#include "serialization.h"

namespace serialization
{
  namespace detail
  {
    template <typename Archive, class T>
    bool serialize_list_element(Archive& ar, T& e)
    {
      return ::do_serialize(ar, e);
    }

    template <typename Archive>
    bool serialize_list_element(Archive& ar, uint64_t& e)
    {
      ar.serialize_varint(e);
      return true;
    }
  }
}

template <template <bool> class Archive, class T>
bool do_serialize(Archive<false> &ar, std::list<T> &l)
{
  size_t cnt;
  ar.begin_array(cnt);
  if (!ar.stream().good())
    return false;
  l.clear();

  // very basic sanity check
  if (ar.remaining_bytes() < cnt) {
    ar.stream().setstate(std::ios::failbit);
    return false;
  }

  for (size_t i = 0; i < cnt; i++) {
    if (i > 0)
      ar.delimit_array();
    l.push_back(T());
    T &t = l.back();
    if (!::serialization::detail::serialize_list_element(ar, t))
      return false;
    if (!ar.stream().good())
      return false;
  }
  ar.end_array();
  return true;
}

template <template <bool> class Archive, class T>
bool do_serialize(Archive<true> &ar, std::list<T> &l)
{
  size_t cnt = l.size();
  ar.begin_array(cnt);
  for (typename std::list<T>::iterator i = l.begin(); i != l.end(); ++i) {
    if (!ar.stream().good())
      return false;
    if (i != l.begin())
      ar.delimit_array();
    if(!::serialization::detail::serialize_list_element(ar, *i))
      return false;
    if (!ar.stream().good())
      return false;
  }
  ar.end_array();
  return true;
}
