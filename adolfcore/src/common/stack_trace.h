//17copyright


#ifndef MONERO_EXCEPTION_H
#define MONERO_EXCEPTION_H

#include <string>

namespace tools
{

void set_stack_trace_log(const std::string &log);
void log_stack_trace(const char *msg);

}  // namespace tools

#endif
