//17copyright   

 
//
// Parts of this file are originally copyright (c) 2012-2013 The Cryptonote developers

#pragma once

#include "rpc/core_rpc_server.h"

#undef MONERO_DEFAULT_LOG_CATEGORY
#define MONERO_DEFAULT_LOG_CATEGORY "daemon"

namespace daemonize
{

class t_rpc final
{
public:
  static void init_options(boost::program_options::options_description & option_spec)
  {
    cryptonote::core_rpc_server::init_options(option_spec);
  }
private:
  cryptonote::core_rpc_server m_server;
public:
  t_rpc(
      boost::program_options::variables_map const & vm
    , t_core & core
    , t_p2p & p2p
    )
    : m_server{core.get(), p2p.get()}
  {
    MGINFO("Initializing core rpc server...");
    if (!m_server.init(vm))
    {
      throw std::runtime_error("Failed to initialize core rpc server.");
    }
    MGINFO("Core rpc server initialized OK on port: " << m_server.get_binded_port());
  }

  void run()
  {
    MGINFO("Starting core rpc server...");
    if (!m_server.run(2, false))
    {
      throw std::runtime_error("Failed to start core rpc server.");
    }
    MGINFO("Core rpc server started ok");
  }

  void stop()
  {
    MGINFO("Stopping core rpc server...");
    m_server.send_stop_signal();
    m_server.timed_wait_server_stop(5000);
  }

  cryptonote::core_rpc_server* get_server()
  {
    return &m_server;
  }

  ~t_rpc()
  {
    MGINFO("Deinitializing rpc server...");
    try {
      m_server.deinit();
    } catch (...) {
      MERROR("Failed to deinitialize rpc server...");
    }
  }
};

}
