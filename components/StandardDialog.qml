//17copyright   

import QtQuick 2.0
import QtQuick.Controls 1.4
import QtQuick.Dialogs 1.2
import QtQuick.Layouts 1.1
import QtQuick.Controls.Styles 1.4
import QtQuick.Window 2.0

import "../components" as MoneroComponents

Window {
    id: root
    modality: Qt.ApplicationModal
    flags: Qt.Window | Qt.FramelessWindowHint
    property alias title: dialogTitle.text
    property alias text: dialogContent.text
    property alias content: root.text
    property alias cancelVisible: cancelButton.visible
    property alias okVisible: okButton.visible
    property alias textArea: dialogContent
    property alias okText: okButton.text
    property alias cancelText: cancelButton.text

    property var icon

    // same signals as Dialog has
    signal accepted()
    signal rejected()

    // Make window draggable
    MouseArea {
        anchors.fill: parent
        property point lastMousePos: Qt.point(0, 0)
        onPressed: { lastMousePos = Qt.point(mouseX, mouseY); }
        onMouseXChanged: root.x += (mouseX - lastMousePos.x)
        onMouseYChanged: root.y += (mouseY - lastMousePos.y)
    }

    function open() {
        show()
    }

    // TODO: implement without hardcoding sizes
    width:  480
    height: 280

    ColumnLayout {
        id: mainLayout
        spacing: 10
        anchors { fill: parent; margins: 35 }

        RowLayout {
            id: column
            //anchors {fill: parent; margins: 16 }
            Layout.alignment: Qt.AlignHCenter

            Label {
                id: dialogTitle
                horizontalAlignment: Text.AlignHCenter
                font.pixelSize: 32
                font.family: "Arial"
                color: "#555555"
            }

        }

        RowLayout {
            TextArea {
                id : dialogContent
                Layout.fillWidth: true
                Layout.fillHeight: true
                font.family: "Arial"
                textFormat: TextEdit.AutoText
                readOnly: true
                font.pixelSize: 12
            }
        }

        // Ok/Cancel buttons
        RowLayout {
            id: buttons
            spacing: 60
            Layout.alignment: Qt.AlignHCenter

            MoneroComponents.StandardButton {
                id: cancelButton
                width: 120
                fontSize: 14
                shadowReleasedColor: "#E60000"
                shadowPressedColor: "#800000"
                releasedColor: "#CC0000"
                pressedColor: "#E60000"
                text: qsTr("Cancel") + translationManager.emptyString
                onClicked: {
                    root.close()
                    root.rejected()
                }
            }

            MoneroComponents.StandardButton {
                id: okButton
                width: 120
                fontSize: 14
                shadowReleasedColor: "#E60000"
                shadowPressedColor: "#800000"
                releasedColor: "#CC0000"
                pressedColor: "#E60000"
                text: qsTr("Ok")
                KeyNavigation.tab: cancelButton
                onClicked: {
                    root.close()
                    root.accepted()

                }
            }
        }
    }

}



