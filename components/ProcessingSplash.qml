//17copyright   

import QtQuick 2.0
import QtQuick.Window 2.1
import QtQuick.Controls 1.4
import QtQuick.Layouts 1.1

Window {
    id: root
    modality: Qt.ApplicationModal
    flags: Qt.Window
    property alias messageText: messageTitle.text
    property alias heightProgressText : heightProgress.text

    width: 200
    height: 100
    opacity: 0.7

    ColumnLayout {
        id: rootLayout

        anchors.left: parent.left
        anchors.right: parent.right
        anchors.verticalCenter: parent.verticalCenter

        anchors.leftMargin: 30
        anchors.rightMargin: 30

        BusyIndicator {
            running: parent.visible
            Layout.alignment: Qt.AlignVCenter | Qt.AlignHCenter
        }

        Text {
            id: messageTitle
            text: "Please wait..."
            font {
                pixelSize: 22
            }
            horizontalAlignment: Text.AlignHCenter
            Layout.alignment: Qt.AlignVCenter | Qt.AlignHCenter
            Layout.fillWidth: true
        }


        Text {
            id: heightProgress
            font {
                pixelSize: 18
            }
            horizontalAlignment: Text.AlignHCenter
            Layout.alignment: Qt.AlignVCenter | Qt.AlignHCenter
            Layout.fillWidth: true
        }
    }
}
