//17copyright   

                                                

import QtQuick 2.0
import moneroComponents.Wallet 1.0

Item {
    id: item
    property int fillLevel: 0
    height: 22
    anchors.margins:15
    visible: false
    //clip: true

    function updateProgress(currentBlock,targetBlock, blocksToSync){
        if(targetBlock == 1) {
            fillLevel = 0
            progressText.text = qsTr("Establishing connection...");
            progressBar.visible = true
            return
        }

        if(targetBlock > 0) {
            var remaining = targetBlock - currentBlock
            // wallet sync
            if(blocksToSync > 0)
                var progressLevel = (100*(blocksToSync - remaining)/blocksToSync).toFixed(0);
            // Daemon sync
            else
                var progressLevel = (100*(currentBlock/targetBlock)).toFixed(0);
            fillLevel = progressLevel
            progressText.text = qsTr("Blocks remaining: %1").arg(remaining.toFixed(0));
            progressBar.visible = currentBlock < targetBlock
        }
    }

    Rectangle {
        id: bar
        anchors.left: parent.left
        anchors.right: parent.right
        anchors.top: parent.top
        height: 22
        radius: 2
        color: "#FFFFFF"

        Rectangle {
            id: fillRect
            anchors.top: parent.top
            anchors.bottom: parent.bottom
            anchors.left: parent.left
            anchors.margins: 2
            height: bar.height
            property int maxWidth: parent.width - 4
            width: (maxWidth * fillLevel) / 100
            color: {
               if(item.fillLevel < 99 ) return "#CC0000"
               //if(item.fillLevel < 99) return "#FFE00A"
                return "#36B25C"
            }

        }

        Rectangle {
            color:"#333"
            anchors.bottom: parent.bottom
            anchors.left: parent.left
            anchors.leftMargin: 8

            Text {
                id:progressText
                anchors.bottom: parent.bottom
                font.family: "Arial"
                font.pixelSize: 12
                color: "#000"
                text: qsTr("Synchronizing blocks")
                height:18
            }
        }
    }

}
