//17copyright   

import QtQuick 2.0
import QtQuick.Controls 1.4
import QtQuick.Dialogs 1.2
import QtQuick.Layouts 1.1
import QtQuick.Controls.Styles 1.4
import QtQuick.Window 2.0

import "../components" as MoneroComponents

Window {
    id: root
    modality: Qt.ApplicationModal
    flags: Qt.Window | Qt.FramelessWindowHint
    property int countDown: 5;
    signal rejected()
    signal started();

    function open() {
        show()
        countDown = 5;
        timer.start();
    }

    // TODO: implement without hardcoding sizes
    width: 480
    height: 200

    // Make window draggable
    MouseArea {
        anchors.fill: parent
        property point lastMousePos: Qt.point(0, 0)
        onPressed: { lastMousePos = Qt.point(mouseX, mouseY); }
        onMouseXChanged: root.x += (mouseX - lastMousePos.x)
        onMouseYChanged: root.y += (mouseY - lastMousePos.y)
    }

    ColumnLayout {
        id: mainLayout
        spacing: 10
        anchors { fill: parent; margins: 35 }

        ColumnLayout {
            id: column
            //anchors {fill: parent; margins: 16 }
            Layout.alignment: Qt.AlignHCenter

            Timer {
                id: timer
                interval: 1000;
                running: false;
                repeat: true
                onTriggered: {
                    countDown--;
                    if(countDown < 0){
                        running = false;
                        // Start daemon
                        root.close()
                        appWindow.startDaemon(persistentSettings.daemonFlags);
                        root.started();
                    }
                }
            }

            Text {
                text: qsTr("Starting Adolf daemon in %1 seconds").arg(countDown);
                font.pixelSize: 18
                Layout.alignment: Qt.AlignHCenter
                Layout.fillWidth: true
                horizontalAlignment: Text.AlignHCenter
            }

        }

        RowLayout {
            id: buttons
            spacing: 60
            Layout.alignment: Qt.AlignHCenter

            MoneroComponents.StandardButton {
                id: okButton
                visible:false
                fontSize: 14
                shadowReleasedColor: "#E60000"
                shadowPressedColor: "#800000"
                releasedColor: "#CC0000"
                pressedColor: "#E60000"
                text: qsTr("Start daemon (%1)").arg(countDown)
                KeyNavigation.tab: cancelButton
                onClicked: {
                    timer.stop();
                    root.close()
                    appWindow.startDaemon(persistentSettings.daemonFlags);
                    root.started()
                }
            }

            MoneroComponents.StandardButton {
                id: cancelButton
                fontSize: 14
                shadowReleasedColor: "#E60000"
                shadowPressedColor: "#800000"
                releasedColor: "#CC0000"
                pressedColor: "#E60000"
                text: qsTr("Use custom settings")

                onClicked: {
                    timer.stop();
                    root.close()
                    root.rejected()
                }
            }
        }
    }
}



