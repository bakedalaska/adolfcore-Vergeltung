//17copyright   

                                                

import QtQuick 2.2
import QtQml 2.2
import QtQuick.Layouts 1.1
import "../components"

ColumnLayout {
    id: page
    signal createWalletClicked()
    signal recoveryWalletClicked()
    signal openWalletClicked()
    opacity: 0
    visible: false
    property int buttonSize: (isMobile) ? 80 : 190
    property int buttonImageSize: (isMobile) ? buttonSize - 10 : buttonSize - 30

    function onPageClosed() {
        // Save settings used in open from file.
        // other wizard settings are saved on last page in applySettings()
        appWindow.persistentSettings.testnet = wizard.settings["testnet"]
        appWindow.persistentSettings.daemon_address = wizard.settings["daemon_address"]
        appWindow.persistentSettings.language = wizard.settings.language
        appWindow.persistentSettings.locale   = wizard.settings.locale

        return true;
    }

    function saveDaemonAddress() {
        wizard.settings["daemon_address"] = daemonAddress.text
        wizard.settings["testnet"] = testNet.checked
    }

    QtObject {
        id: d
        readonly property string daemonAddressTestnet : "localhost:38081"
        readonly property string daemonAddressMainnet : "localhost:14881"
    }

    Behavior on opacity {
        NumberAnimation { duration: 100; easing.type: Easing.InQuad }
    }

    onOpacityChanged: visible = opacity !== 0

    ColumnLayout {
        id: headerColumn
        Layout.leftMargin: wizardLeftMargin
        Layout.rightMargin: wizardRightMargin
        Layout.bottomMargin: (!isMobile) ? 40 : 20
        spacing: 30

        Text {
            Layout.fillWidth: true
            font.family: "Arial"
            font.pixelSize: 28
            //renderType: Text.NativeRendering
            color: "#3F3F3F"
            wrapMode: Text.Wrap
            horizontalAlignment: Text.AlignHCenter
            text: qsTr("Welcome to Adolfcoin!") + translationManager.emptyString
        }

        Text {
            Layout.fillWidth: true
            font.family: "Arial"
            font.pixelSize: 18
            //renderType: Text.NativeRendering
            color: "#4A4646"
            wrapMode: Text.Wrap
            horizontalAlignment: Text.AlignHCenter
            text: qsTr("Please select one of the following options:") + translationManager.emptyString
        }
    }

    GridLayout {
        Layout.leftMargin: wizardLeftMargin
        Layout.rightMargin: wizardRightMargin
        Layout.alignment: Qt.AlignCenter
        id: actionButtons
        columnSpacing: 40
        rowSpacing: 10
        Layout.fillWidth: true
        Layout.fillHeight: true
        flow: isMobile ? GridLayout.TopToBottom : GridLayout.LeftToRight

        GridLayout {
            Layout.fillHeight: true
            Layout.fillWidth: true
            flow: !isMobile ? GridLayout.TopToBottom : GridLayout.LeftToRight
            rowSpacing: 20
            columnSpacing: 10

            Rectangle {
                Layout.preferredHeight: page.buttonSize
                Layout.preferredWidth: page.buttonSize
                radius: page.buttonSize
                color: createWalletArea.containsMouse ? "#DBDBDB" : "#FFFFFF"


                Image {
                    width: page.buttonImageSize
                    height: page.buttonImageSize
                    fillMode: Image.PreserveAspectFit
                    horizontalAlignment: Image.AlignRight
                    verticalAlignment: Image.AlignTop
                    anchors.centerIn: parent
                    source: "qrc:///images/createWallet.png"
                }

                MouseArea {
                    id: createWalletArea
                    anchors.fill: parent
                    hoverEnabled: true
                    onClicked: {
                        page.saveDaemonAddress()
                        page.createWalletClicked()
                    }
                }
            }

            Text {
                Layout.preferredWidth: 190
                font.family: "Arial"
                font.pixelSize: 16
                color: "#4A4949"
                horizontalAlignment: Text.AlignHCenter
                wrapMode: Text.WordWrap
                text: qsTr("Create a new wallet") + translationManager.emptyString
            }
        }

        GridLayout {
            Layout.fillWidth: true
            Layout.fillHeight: true
            flow: !isMobile ? GridLayout.TopToBottom : GridLayout.LeftToRight
            rowSpacing: 20
            columnSpacing: 10

            Rectangle {
                Layout.preferredHeight: page.buttonSize
                Layout.preferredWidth:  page.buttonSize
                radius: page.buttonSize
                color: recoverWalletArea.containsMouse ? "#DBDBDB" : "#FFFFFF"

                Image {
                    width: page.buttomImageSize
                    height: page.buttonImageSize
                    fillMode: Image.PreserveAspectFit
                    anchors.centerIn: parent
                    source: "qrc:///images/recoverWallet.png"
                }

                MouseArea {
                    id: recoverWalletArea
                    anchors.fill: parent
                    hoverEnabled: true
                    onClicked: {
                        page.saveDaemonAddress()
                        page.recoveryWalletClicked()
                    }
                }
            }

            Text {
                Layout.preferredWidth: 190
                font.family: "Arial"
                font.pixelSize: 16
                color: "#4A4949"
                horizontalAlignment: Text.AlignHCenter
                text: qsTr("Restore wallet from keys or mnemonic seed") + translationManager.emptyString
                width:page.buttonSize
                wrapMode: Text.WordWrap
            }
        }

        GridLayout {
            Layout.fillHeight: true
            Layout.fillWidth: true
            flow: !isMobile ? GridLayout.TopToBottom : GridLayout.LeftToRight
            rowSpacing: 20
            columnSpacing: 10

            Rectangle {
                Layout.preferredHeight: page.buttonSize
                Layout.preferredWidth:  page.buttonSize
                radius: page.buttonSize
                color: openWalletArea.containsMouse ? "#DBDBDB" : "#FFFFFF"

                Image {
                    width: page.buttonImageSize
                    height: page.buttonImageSize
                    fillMode: Image.PreserveAspectFit
                    anchors.centerIn: parent
                    source: "qrc:///images/openAccount.png"
                }

                MouseArea {
                    id: openWalletArea
                    anchors.fill: parent
                    hoverEnabled: true
                    onClicked: {
                        page.saveDaemonAddress()
                        page.openWalletClicked()
                    }
                }
            }

            Text {
                Layout.preferredWidth: 190
                font.family: "Arial"
                font.pixelSize: 16
                color: "#4A4949"
                horizontalAlignment: Text.AlignHCenter
                text: qsTr("Open a wallet from file") + translationManager.emptyString
                wrapMode: Text.WordWrap
            }
        }



    }

    // daemon select
    // TODO: Move to separate page

    ColumnLayout {
        Layout.leftMargin: wizardLeftMargin
        Layout.rightMargin: wizardRightMargin
        Layout.alignment: Qt.AlignCenter


        Label {
            Layout.topMargin: 20
            fontSize: 14
            text: qsTr("Custom daemon address (optional)") + translationManager.emptyString
                  + translationManager.emptyString
        }

        GridLayout {
            Layout.fillWidth: true
            Layout.alignment: Qt.AlignHCenter

            columnSpacing: 20
            rowSpacing: 20
            flow: isMobile ? GridLayout.TopToBottom : GridLayout.LeftToRight

            RowLayout {
                spacing: 20
                Layout.alignment: Qt.AlignCenter

                LineEdit {
                    id: daemonAddress
                    Layout.alignment: Qt.AlignCenter
                    width: 200
                    fontSize: 14
                    text: {
                        if(appWindow.persistentSettings.daemon_address)
                            return appWindow.persistentSettings.daemon_address;
                        return testNet.checked ? d.daemonAddressTestnet : d.daemonAddressMainnet
                    }

                }

                CheckBox {
                    id: testNet
                    Layout.alignment: Qt.AlignCenter
                    anchors.verticalCenter: parent.verticalCenter
                    text: qsTr("Testnet") + translationManager.emptyString
                    background: "#FFFFFF"
                    fontColor: "#4A4646"
                    fontSize: 16
                    checkedIcon: "../images/checkedVioletIcon.png"
                    uncheckedIcon: "../images/uncheckedIcon.png"
                    checked: appWindow.persistentSettings.testnet;
                }
            }
        }
    }

}

